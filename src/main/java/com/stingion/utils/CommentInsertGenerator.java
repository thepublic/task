package com.stingion.utils;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class CommentInsertGenerator {
    public static void main(String[] args) {

        SecureRandom random = new SecureRandom();
        Random randomInt = new Random();

        for (int i = 0; i < 180; i++) {
            String title = new BigInteger(randomInt.nextInt(200) + 10, random).toString(32);
            String comment = new BigInteger(randomInt.nextInt(450) + 15, random).toString(32);
            String name = new BigInteger(randomInt.nextInt(150) + 5, random).toString(32);
            int rating = randomInt.nextInt(5) + 1;
            String ip = (randomInt.nextInt(255) + 1) + "." + (randomInt.nextInt(255) + 1) + "."
                    + (randomInt.nextInt(255) + 1) + "." + (randomInt.nextInt(255) + 1);

            long offset = Timestamp.valueOf("2016-01-01 00:00:00").getTime();
            long end = Timestamp.valueOf("2016-01-09 00:00:00").getTime();
            long diff = end - offset + 1;
            Timestamp rand = new Timestamp(offset + (long) (Math.random() * diff));
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            System.out.println("INSERT INTO \"Comment\" VALUES (comment_id_seq.nextval, '" + title + "','"
                    + comment + "','" + name + "'," + rating + ",'" + ip + "'," +
                     "TO_TIMESTAMP('" + format.format(new Date(rand.getTime())) + "', 'YYYY-MM-DD HH24:MI:SS.FF')" + ");");

        }
    }
}
