package com.stingion.validation;

import com.stingion.data.model.Comment;
import com.stingion.data.model.modelTypes.RatingScope;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class CommentValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Comment.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Comment comment = (Comment) target;

        Integer rating = comment.getRating();

        ValidationUtils.rejectIfEmpty(errors, "title", "comment.title.empty");
        ValidationUtils.rejectIfEmpty(errors, "comment", "comment.name.empty");
        ValidationUtils.rejectIfEmpty(errors, "name", "comment.naming.empty");
        ValidationUtils.rejectIfEmpty(errors, "rating", "comment.rating.empty");

        if (comment == null || rating == null)
            rating = -1;
        if (rating < 1 || rating > 5)
            errors.rejectValue("rating", "rating.number.lessThenOneMoreThenFive");
    }
}
