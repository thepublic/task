package com.stingion.data.service;

import com.stingion.data.exception.CommentNotFoundException;
import com.stingion.data.model.Comment;
import com.stingion.data.repository.CommentRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Resource
    private CommentRepository commentRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public Comment create(Comment comment) {
        Comment createdComment = comment;
        return commentRepository.save(createdComment);
    }

    @Override
    @Transactional
    public Comment findById(int id) {
        return commentRepository.findOne(id);
    }

    @Override
    @Transactional(rollbackFor = CommentNotFoundException.class)
    public Comment delete(int id) throws CommentNotFoundException {
        Comment deletedComment = commentRepository.findOne(id);

        if (deletedComment == null)
            throw new CommentNotFoundException();

        commentRepository.delete(deletedComment);
        return deletedComment;
    }

    @Override
    @Transactional
    public List<Comment> findAll() {
        return commentRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor = CommentNotFoundException.class)
    public Comment update(Comment comment) throws CommentNotFoundException {
        Comment updatedComment = commentRepository.findOne(comment.getId());

        if (updatedComment == null)
            throw new CommentNotFoundException();

        updatedComment.setTitle(comment.getTitle());
        updatedComment.setComment(comment.getComment());
        updatedComment.setName(comment.getName());
        updatedComment.setRating(comment.getRating());
        updatedComment.setIp(comment.getIp());
        updatedComment.setDate(comment.getDate());

        return updatedComment;
    }

    @Override
    @Transactional
    public Boolean checkIfIpAdded(String ip) {
        return entityManager.createNamedQuery("getAllIPs").getResultList().contains(ip.toLowerCase());
    }
}
