package com.stingion.data.service;

import com.stingion.data.exception.CommentNotFoundException;
import com.stingion.data.model.Comment;

import java.util.List;

public interface CommentService {
    public Comment create(Comment comment);
    public Comment delete(int id) throws CommentNotFoundException;
    public List<Comment> findAll();
    public Comment update(Comment comment) throws CommentNotFoundException;
    public Comment findById(int id);
    public Boolean checkIfIpAdded(String ip);
}
