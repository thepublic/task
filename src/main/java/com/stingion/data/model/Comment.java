package com.stingion.data.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

@NamedQueries({
        @NamedQuery(
                name = "getAllIPs",
                query = "SELECT c.ip FROM Comment c"
        )
})
@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Table(name = "\"Comment\"")
public class Comment {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comment_autogen_id_gen")
    @SequenceGenerator(name = "comment_autogen_id_gen", sequenceName = "comment_id_seq", allocationSize = 1)
    private Integer id;

    private String title;

    @Column(name = "\"comment\"")
    private String comment;

    private String name;

    private Integer rating;

    private String ip;

    @Column(name = "\"date\"")
    private Date date;
}
