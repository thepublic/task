/*
 * Created under not commercial project
 */

package com.stingion.data.model.modelTypes;

public enum RatingScope {
    Zero, One, Two, Three, Four, Five;
}
