package com.stingion.controller;

import com.stingion.data.exception.CommentNotFoundException;
import com.stingion.data.model.Comment;
import com.stingion.data.service.CommentService;
import com.stingion.validation.CommentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping(value = "/comment")
public class CommentController {

    @Autowired
    MessageSource messageSource;

    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentValidator commentValidator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(commentValidator);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView commentListPage(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("comment-list");
        List<Comment> commentList = commentService.findAll();
        mav.addObject("commentList", commentList);
        mav.addObject("ipAdded", commentService.checkIfIpAdded(request.getRemoteAddr()));
        return mav;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView newCommentPage(HttpServletRequest request) {
        if (commentService.checkIfIpAdded(request.getRemoteAddr())) {
            ModelAndView mav = new ModelAndView();
            mav.setViewName("redirect:/comment/list");
            return mav;
        }
        ModelAndView mav = new ModelAndView("comment-new", "comment", new Comment());
        return mav;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView createNewComment(@ModelAttribute @Valid Comment comment,
                                         BindingResult result,
                                         HttpServletRequest request,
                                         final RedirectAttributes redirectAttributes) {

        if (result.hasErrors())
            return new ModelAndView("comment-new");

        //Temp
        comment.setIp(request.getRemoteAddr());
        comment.setDate(new Date());

        ModelAndView mav = new ModelAndView();
        String message = messageSource.getMessage("newCommentWasSuccessfullyCreated",
                new Object[]{comment.getComment()}, LocaleContextHolder.getLocale());


        commentService.create(comment);
        mav.setViewName("redirect:/comment/list");

        redirectAttributes.addFlashAttribute("message", message);
        return mav;
    }

    @RequestMapping(value = "/manager/list", method = RequestMethod.GET)
    public ModelAndView commentMangerListPage(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("comment-list-manager");
        List<Comment> commentList = commentService.findAll();
        mav.addObject("commentList", commentList);
        mav.addObject("ipAdded", commentService.checkIfIpAdded(request.getRemoteAddr()));
        return mav;
    }

    @RequestMapping(value = "/manager/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteComment(@PathVariable Integer id,
                                      final RedirectAttributes redirectAttributes) throws CommentNotFoundException {

        ModelAndView mav = new ModelAndView("redirect:/comment/manager/list");

        Comment comment = commentService.delete(id);
        String message = messageSource.getMessage("commentWasSuccessfullyDeleted",
                new Object[]{comment.getComment()}, LocaleContextHolder.getLocale());

        redirectAttributes.addFlashAttribute("message", message);
        return mav;
    }
}
