/**
 * Created under not commercial project
 */
package com.stingion.controller;

import com.stingion.data.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProbeController {

    public static final String MESSAGE_WORD = "message";

    @Autowired
    CommentService service;

    @Autowired
    MessageSource messageSource;

    @RequestMapping(value = {"/", "probe"}, method = RequestMethod.GET)
    public ModelAndView probe() {
        ModelAndView mav = new ModelAndView("probe");
        String message = messageSource.getMessage("probe", null, LocaleContextHolder.getLocale());
        mav.addObject(MESSAGE_WORD, message);
        return mav;
    }

    @RequestMapping(value = {"/", "hello"}, method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView mav = new ModelAndView("probe");
        mav.addObject(MESSAGE_WORD, "Hello World!");
        return mav;
    }

    @RequestMapping(value = {"/", "other"}, method = RequestMethod.GET)
    public ModelAndView other() {
        ModelAndView mav = new ModelAndView("probe");
        mav.addObject(MESSAGE_WORD, "Any other message");
        return mav;
    }
}
