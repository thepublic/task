<%@include file="/WEB-INF/pages/common/taglibs.jsp" %>
<%--
  ~ Created under not commercial project
  --%>
<spring:message code="newCommentPage" var="newCommentPageVar"/>

<t:concretpage title="${newCommentPageVar}">
            <form:form method="POST" commandName="comment" action="${pageContext.request.contextPath}/comment/create" >
        <table>
            <tbody>
                <tr>
                    <td><spring:message code="title" />:</td>
                    <td><form:input path="title" /></td>
                    <td><form:errors path="title" cssStyle="color: red;"/></td>
                </tr>
                <tr>
                    <td><spring:message code="comment" />:</td>
                    <td><form:input path="comment" /></td>
                    <td><form:errors path="comment" cssStyle="color: red;"/></td>
                </tr>
                <tr>
                    <td><spring:message code="username" />:</td>
                    <td><form:input path="name" /></td>
                    <td><form:errors path="name" cssStyle="color: red;"/></td>
                </tr>
                <tr>
                    <td><spring:message code="rating" />:</td>
                    <td><form:input path="rating" /></td>
                    <td><form:errors path="rating" cssStyle="color: red;"/></td>
                </tr>
                <tr>
                    <spring:message code="create" var="createVar"/>
                    <td><input type="submit" value="${createVar}"/></td>
                    <td>
                        <spring:message code="cancel" var="cancelVar"/>
                        <input type="button" name="cancel" value="${cancelVar}"
                               onClick="window.location='${pageContext.request.contextPath}/comment/list';"/>
                    </td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        </form:form>
</t:concretpage>