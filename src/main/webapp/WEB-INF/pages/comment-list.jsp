<%@include file="/WEB-INF/pages/common/taglibs.jsp" %>

<style type="text/css">
    table {
        table-layout: fixed;
        word-wrap: break-word;
    }
</style>

<spring:message code="commentListPage" var="commentListPageVar"/>

<t:concretpage title="${commentListPageVar}">
    <i>${message}</i><br/>
    <c:if test="${ipAdded==false}">
        <a href="${pageContext.request.contextPath}/comment/create"><spring:message code="createNewComment" /></a><br/>
    </c:if>
    <table border="1px" cellpadding="0" cellspacing="0" summary="Comment List Table" id="comments"
           class="table table-bordered table-condensed table-striped table-striped sortable-theme-bootstrap">
        <thead>
        <tr style="vertical-align: middle;">
            <th style="text-align: center;" width="150px"><a href="#"><spring:message code="title" /></a></th>
            <th style="text-align: center;" width="150px"><a href="#"><spring:message code="comment" /></a></th>
            <th style="text-align: center;" width="150px"><a href="#"><spring:message code="username" /></a></th>
            <th style="text-align: center;" width="150px"><a href="#"><spring:message code="rating" /></a></th>
            <th style="text-align: center;" width="150px"><a href="#"><spring:message code="date" /></a></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="comment" items="${commentList}">
            <tr>
                <td style="vertical-align: middle;">${comment.title}</td>
                <td style="vertical-align: middle;">${comment.comment}</td>
                <td style="vertical-align: middle;text-align: center;">${comment.name}</td>
                <td style="vertical-align: middle;text-align: center;">${comment.rating}</td>
                <td style="vertical-align: middle;text-align: center;">${comment.date}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</t:concretpage>

<script>
    $(document).ready(function() {
        $('#comments').dataTable();
    });
</script>
