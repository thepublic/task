<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="footer">
    <p><spring:message code="createdUnderNonCommercialProject" /></p>
</div>
