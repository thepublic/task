<%@include file="/WEB-INF/pages/common/taglibs.jsp" %>
<%--
  ~ Created under not commercial project
  --%>

<style type="text/css">
    table {
        table-layout: fixed;
        word-wrap: break-word;
    }
</style>

<spring:message code="commentListPage" var="commentListPageVar"/>

<t:concretpage title="${commentListPageVar}">
    <i>${message}</i><br/>
    <table border="1px" cellpadding="0" cellspacing="0" summary="Comment List Table" id="comments"
           class="table table-bordered table-condensed table-striped table-striped sortable-theme-bootstrap">
        <thead>
        <tr style="vertical-align: middle;">
            <th style="text-align: center;" width="25px"><a href="#"><spring:message code="id" /></a></th>
            <th style="text-align: center;" width="150px"><a href="#"><spring:message code="title" /></a></th>
            <th style="text-align: center;" width="150px"><a href="#"><spring:message code="comment" /></a></th>
                <th style="text-align: center;" width="150px"><a href="#"><spring:message code="username" /></a></th>
            <th style="text-align: center;" width="150px"><a href="#"><spring:message code="rating" /></a></th>
            <th style="text-align: center;" width="50px"><a href="#"><spring:message code="ip" /></a></th>
            <th style="text-align: center;" width="150px"><a href="#"><spring:message code="date" /></a></th>
                <th style="text-align: center;" width="50px"><spring:message code="actions" /></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="comment" items="${commentList}">
            <tr>
                <td style="vertical-align: middle;text-align: center;">${comment.id}</td>
                <td style="vertical-align: middle;">${comment.title}</td>
                <td style="vertical-align: middle;">${comment.comment}</td>
                <td style="vertical-align: middle;text-align: center;">${comment.name}</td>
                <td style="vertical-align: middle;text-align: center;">${comment.rating}</td>
                <td style="vertical-align: middle;text-align: center;">${comment.ip}</td>
                <td style="vertical-align: middle;text-align: center;">${comment.date}</td>
                <td style="vertical-align: middle;text-align: center;">
                    <spring:message code="delete" var="deleteVar"/>
                    <a href="${pageContext.request.contextPath}/comment/manager/delete/${comment.id}"
                       onclick="return confirm('${deleteVar} \'${comment.comment}\'?');">
                        <spring:message code="delete" /></a><br/>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</t:concretpage>

<script>
    $(document).ready(function() {
        $('#comments').dataTable();
    });
</script>
