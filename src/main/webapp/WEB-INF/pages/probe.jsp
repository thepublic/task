<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/pages/common/taglibs.jsp" %>

<t:concretpage title="${message}">
    <p>
        <div align="center">
            <h1>${message}</h1>
        </div>
    </p>
</t:concretpage>
