CREATE TABLE "Comment" (
    id INT NOT NULL,
    title VARCHAR(100) NOT NULL,
    "comment" VARCHAR(500) NOT NULL,
    name VARCHAR(50),
    rating INT NOT NULL,
    ip VARCHAR(15) NOT NULL UNIQUE,
    CONSTRAINT comment_PK PRIMARY KEY (ID)
    ENABLE
);

declare
    comment_first_gen_id INTEGER;
begin
   select max(id)
   into comment_first_gen_id
   from "Comment";
   IF comment_first_gen_id is null THEN
     comment_first_gen_id := 1  ;
   ELSE
     comment_first_gen_id := comment_first_gen_id + 1;
   END IF;

   execute immediate 'Create sequence comment_id_seq start with '
                      || comment_first_gen_id || ' increment by 1';
end;
